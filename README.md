# PHP IPA Parser

Identify the Meta info about the Apple `.ipa` file easily without extra efforts

### Installation

```
composer require saiashirwadinformatia/ipa-parser
```

### Usage

```php
use SaiAshirwadInformatia\Services\IPAParser;

$path = __DIR__ . 'example.ipa';
$ipa = IPAParser::load($path);

echo $ipa->name;        // Name of App
echo $ipa->identifier;  // Bundle Identifier of App
echo $ipa->versionName; // Vesion Name of App
echo $ipa->versionCode; // Vesion Code of App
echo $ipa->primaryIcon; // Primary Icon File Contents of App

var_dump($ipa->info);   // Prints detailed plist info
var_dump($ipa->info->build); // Prints build detail info
var_dump($ipa->info->permissions); // Prints build detail info
var_dump($ipa->info->orientations); // Prints build detail info
var_dump($ipa->icons);  // Prints detailed icons
var_dump($ipa->icons->iphone); // Get all iPhone icons
```

## Contribute

1. Fork this project
1. Clone your repository
1. Create new branch of bug-fixes/new-features

#### Dependencies

```
composer install
```

#### Run Tests

```
./vendor/bin/phpunit
```
