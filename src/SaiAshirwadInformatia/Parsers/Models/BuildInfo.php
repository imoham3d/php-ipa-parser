<?php

namespace SaiAshirwadInformatia\Parsers\Models;

use CFPropertyList\CFPropertyList;

class BuildInfo
{
    /**
     * @var mixed
     */
    protected $compiler;

    /**
     * @var mixed
     */
    protected $platformBuild;

    /**
     * @var mixed
     */
    protected $platformName;

    /**
     * @var mixed
     */
    protected $platformVersion;

    /**
     * @var mixed
     */
    protected $sdkBuild;

    /**
     * @var mixed
     */
    protected $sdkName;

    /**
     * @var mixed
     */
    protected $xcode;

    /**
     * @var mixed
     */
    protected $xcodeBuild;

    /**
     * @var mixed
     */
    protected $packageType;

    /**
     * @var mixed
     */
    protected $developmentRegion;

    /**
     * @var mixed
     */
    protected $launchScreen;

    /**
     * @var mixed
     */
    protected $mainStoryBoard;

    /**
     * @var mixed
     */
    protected $executable;

    /**
     * @param CFPropertyList $infoPlist
     */
    public function __construct($infoPlistArray)
    {
        $keys = [
            'DTCompiler'                => 'compiler',
            'DTPlatformBuild'           => 'platformBuild',
            'DTPlatformName'            => 'platformName',
            'DTPlatformVersion'         => 'platformVersion',
            'DTSDKBuild'                => 'sdkBuild',
            'DTSDKName'                 => 'sdkName',
            'DTXcode'                   => 'xcode',
            'DTXcodeBuild'              => 'xcodeBuild',
            'CFBundlePackageType'       => 'packageType',
            'CFBundleDevelopmentRegion' => 'developmentRegion',
            'UILaunchStoryboardName'    => 'launchScreen',
            'UIMainStoryboardFile'      => 'mainStoryBoard',
            'CFBundleExecutable'        => 'executable',
        ];
        foreach ($keys as $key => $prop) {
            if (isset($infoPlistArray[$key])) {
                $this->$prop = $infoPlistArray[$key]->getValue();
            }
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->$key ?? null;
    }
}
