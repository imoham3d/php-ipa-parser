<?php

namespace SaiAshirwadInformatia\Parsers\Models;

use CFPropertyList\CFPropertyList;
use SaiAshirwadInformatia\Parsers\Models\Icon;

class IPA
{
    /**
     * @var mixed
     */
    protected $info;

    /**
     * @var mixed
     */
    protected $icons;

    /**
     * @param CFPropertyList $infoPlist
     */
    public function __construct(string $appDir)
    {
        $plist      = new CFPropertyList($appDir . 'info.plist', CFPropertyList::FORMAT_XML);
        $this->info = new Info($plist);

        $info = $plist->getValue()->getValue();

        $icons = [
            'CFBundleIcons'      => 'iphone',
            'CFBundleIcons~ipad' => 'ipad',
        ];
        $variations  = ['', '@2x', '@3x'];
        $this->icons = new \stdClass;
        foreach ($icons as $key => $for) {
            if (!isset($info[$key])) {
                continue;
            }
            if (!isset($this->icons->$for)) {
                $this->icons->$for = [];
            }
            $iconArray = $info[$key]->getValue();
            if (!isset($iconArray['CFBundlePrimaryIcon'])) {
                continue;
            }
            $primaryIcons = $iconArray['CFBundlePrimaryIcon']->getValue();
            $bundleIcons  = $primaryIcons['CFBundleIconFiles']->getValue();

            foreach ($bundleIcons as $iconName) {
                $iconName = $iconName->getValue();
                foreach ($variations as $variation) {
                    $iconName = $iconName . $variation;
                    if ($for == 'ipad') {
                        $iconName .= '~ipad';
                    }
                    $path = $appDir . $iconName . '.png';
                    if (!file_exists($path)) {
                        continue;
                    }
                    $this->icons->$for[$iconName] = new Icon($iconName, file_get_contents($path));
                }
            }
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        if ($key == 'primaryIcon') {
            if (isset($this->icons->iphone)) {
                $first = array_key_first($this->icons->iphone);
                return $this->icons->iphone[$first]->content;
            } else if (isset($this->icons->ipad)) {
                $first = array_key_first($this->icons->ipad);
                return $this->icons->ipad[$first]->content;
            }
            throw new \Exception("Primary icon not available");
        }
        $allowed = ['name', 'identifier', 'versionName', 'versionCode', 'info', 'icons'];
        if (!in_array($key, $allowed)) {
            return null;
        }
        if ($key == 'info' || $key == 'icons') {
            return $this->$key;
        }
        return $this->info->$key;
    }
}
