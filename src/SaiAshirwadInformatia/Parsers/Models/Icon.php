<?php

namespace SaiAshirwadInformatia\Parsers\Models;

class Icon
{
    /**
     * @var mixed
     */
    protected $name;

    /**
     * @var mixed
     */
    protected $content;

    /**
     * @param $name
     * @param $content
     */
    public function __construct($name, $content)
    {
        $this->name    = $name;
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    protected function getTempFile()
    {
        $path = tmpfile();
        file_put_contents($path, $this->contents);
        return $path;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        if (isset($this->$key)) {
            return $this->$key;
        }
        if ($key == 'image_base64') {
            return base64_encode($this->content);
        }
        if ($key == 'tmp_file') {
            return $this->getTempFile();
        }
        return null;
    }
}
