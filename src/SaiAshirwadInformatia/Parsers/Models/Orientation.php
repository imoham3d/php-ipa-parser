<?php

namespace SaiAshirwadInformatia\Parsers\Models;

class Orientation
{
    /**
     * @var mixed
     */
    private $portrait;

    /**
     * @var mixed
     */
    private $portraitUpsideDown;

    /**
     * @var mixed
     */
    private $landscapeLeft;

    /**
     * @var mixed
     */
    private $landscapeRight;

    /**
     * @param $orientationsArray
     */
    public function __construct($orientations)
    {
        $orientationsArray = $orientations->getValue();
        $values            = [
            'UIInterfaceOrientationPortrait'           => 'portrait',
            'UIInterfaceOrientationPortraitUpsideDown' => 'portraitUpsideDown',
            'UIInterfaceOrientationLandscapeLeft'      => 'landscapeLeft',
            'UIInterfaceOrientationLandscapeRight'     => 'landscapeRight',
        ];
        foreach ($values as $value => $prop) {
            $this->$prop = in_array($value, $orientationsArray);
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->$key ?? false;
    }
}
