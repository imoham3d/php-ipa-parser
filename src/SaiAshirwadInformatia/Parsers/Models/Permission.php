<?php

namespace SaiAshirwadInformatia\Parsers\Models;

class Permission
{
    /**
     * @var mixed
     */
    private $name;

    /**
     * @var mixed
     */
    private $description;

    /**
     * @param $name
     * @param $description
     */
    public function __construct($name, $description)
    {
        $this->name        = $name;
        $this->description = $description;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->$key ?? null;
    }
}
