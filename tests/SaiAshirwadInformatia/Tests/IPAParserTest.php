<?php

namespace SaiAshirwadInformatia\Tests;

use PHPUnit\Framework\TestCase;
use SaiAshirwadInformatia\Parsers\Services\IPAParser;

class IPAParserTest extends TestCase
{
    public function testLoadIPA()
    {
        $ipa = IPAParser::load('tests/SaiAshirwadInformatia/Tests/IncontroMRKoye.ipa');

        $this->assertNotNull($ipa->name, "Name is null");
        $this->assertNotNull($ipa->identifier, "Identifier is null");
        $this->assertNotNull($ipa->versionName, "Version Name is null");
        $this->assertNotNull($ipa->versionCode, "Version Code is null");
        $this->assertNotNull($ipa->primaryIcon, "App Icon is null");
        $this->assertNotNull($ipa->info, "Info is null");
    }
}
